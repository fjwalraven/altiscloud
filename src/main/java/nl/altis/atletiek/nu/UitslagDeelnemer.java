package nl.altis.atletiek.nu;

import java.util.ArrayList;
import java.util.List;

public class UitslagDeelnemer {

	private String categorie;
	private String subCat;
	private String naam;
	private String vereniging;
	private String wind;
	private List<String> onderdelen = new ArrayList<>();
	private List<String> waarden = new ArrayList<>();
	
	
	public String getCategorie() {
		return categorie;
	}
	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}
	public String getNaam() {
		return naam;
	}
	public void setNaam(String naam) {
		this.naam = naam;
	}
	public String getVereniging() {
		return vereniging;
	}
	public void setVereniging(String vereniging) {
		this.vereniging = vereniging;
	}
	
	public List<String> getWaarden() {
		return waarden;
	}
	public void setWaarden(List<String> waarden) {
		this.waarden = waarden;
	}
	
	public List<String> getOnderdelen() {
		return onderdelen;
	}
	public void setOnderdelen(List<String> onderdelen) {
		this.onderdelen = onderdelen;
	}
	public String getCat() {
		return subCat;
	}
	public void setCat(String cat) {
		this.subCat = cat;
	}
	
	
	public String getWind() {
		return wind;
	}
	public void setWind(String wind) {
		this.wind = wind;
	}
	@Override
	public String toString() {
		return "Uitslag [categorie=" + categorie + ", cat=" + subCat + ", naam=" + naam + ", vereniging=" + vereniging
				+ ", wind=" + wind + ", onderdelen=" + onderdelen + ", waarden=" + waarden + "]";
	}
	
	
		
}
