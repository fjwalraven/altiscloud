package nl.altis.util;

import java.util.Arrays;
import java.util.List;

public class CategorieUtil {

	private static List<String> cats = Arrays.asList("MPD", "JPD", "MPC", "JPC", "MPB", "JPB", "MPA", "JPA", "MD", "JD",
			"MC", "JC", "MB", "JB", "MA", "JA", "Senioren vrouwen", "Senioren mannen", "V", "M", "V35", "M35", "V40", "M40",
			"V45", "M45", "V50", "M50", "V55", "M55", "V60", "M60", "V65", "M65", "V70", "M70", "V75", "M75", "V80",
			"M80", "Vmas", "Mmas");
	
	private static List<String> pupillen = Arrays.asList("MPD", "JPD", "MPC", "JPC", "MPB", "JPB", "MPA", "JPA");
			
	private static List<String> junioren = Arrays.asList("MD", "JD", "MC", "JC", "MB", "JB", "MA", "JA");
	
	private static List<String> senioren = Arrays.asList("Senioren vrouwen", "Senioren mannen", "V", "M");
	
	private static List<String> masters = Arrays.asList("V35", "M35", "V40", "M40",
			"V45", "M45", "V50", "M50", "V55", "M55", "V60", "M60", "V65", "M65", "V70", "M70", "V75", "M75", "V80",
			"M80", "Vmas", "Mmas");
	
	public static List<String> getCats(){
		return cats;
	}
	
	public static List<String> getPupillen(){
		return pupillen;
	}
	
	public static List<String> getJunioren(){
		return junioren;
	}
	
	public static List<String> getSenioren(){
		return senioren;
	}
	
	public static List<String> getMasters(){
		return masters;
	}
	
	public static int max(int a, int b, int c, int d) {

	    int max = a;

	    if (b > max)
	        max = b;
	    if (c > max)
	        max = c;
	    if (d > max)
	        max = d;

	     return max;
	}

}
