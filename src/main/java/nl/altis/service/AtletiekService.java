package nl.altis.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import nl.altis.dao.AtleetDao;
import nl.altis.dao.ResultaatDao;
import nl.altis.domein.Atleet;
import nl.altis.domein.Resultaat;

/**
 * Session Bean implementation class AtletiekService
 */
@Stateless
@LocalBean
public class AtletiekService {
	
	@Inject
	private AtleetDao atleetDao;
	
	@Inject
	private ResultaatDao resultaatDao;
	
	@PersistenceContext (unitName="AltisBesten")
	private EntityManager em;
	
	public Atleet readAtleet(String id){
		return atleetDao.zoekAtleetById(id, em);
	}
	
	public List<Resultaat> getResultatenAtleet(String naam){
		return resultaatDao.getDeelnemerResultaten(naam, em);
	}
	
	public List<Resultaat> getResultatenPeriode(){
		String datumVan = "2018-03-31";
		String datumTot = "2018-10-10";
		
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date datumVanaf;
		Date datumTotEnMet;
		try {
			datumVanaf = formatter.parse(datumVan);
			datumTotEnMet = formatter.parse(datumTot);
			List<Resultaat> deelnemerResultaten = resultaatDao.getResultatenByPeriode(datumVanaf, datumTotEnMet);
			return deelnemerResultaten;	
		} catch (ParseException e) {
			throw new IllegalArgumentException(e);
		}
		
	}

    /**
     * Default constructor. 
     */
    public AtletiekService() {
        // TODO Auto-generated constructor stub
    }

}
