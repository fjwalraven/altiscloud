package nl.altis.dao;

import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class PersistenceDao {
	
	
	
	public static Boolean isProductieDatabase() {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("AltisBesten");
		EntityManager entityManager = emf.createEntityManager();
		Map<String, Object> properties = emf.getProperties();
		
		String url = (String)properties.get("javax.persistence.jdbc.url");
		if (url.contains("altis_test")){
			return Boolean.FALSE;
		} else {
			return Boolean.TRUE;	
		}		
	}
}
