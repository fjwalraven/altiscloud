package nl.altis.dao;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import nl.altis.atletiek.nu.Categorie;
import nl.altis.atletiek.nu.MeetEenheid;
import nl.altis.atletiek.nu.OnderdeelAnu;
import nl.altis.atletiek.nu.Ronde;
import nl.altis.atletiek.nu.UitslagDeelnemer;
import nl.altis.domein.Atleet;
import nl.altis.domein.Onderdeel;
import nl.altis.domein.Resultaat;
import nl.altis.domein.Wedstrijd;

public class ResultaatDao {
	
	public Resultaat getResultaatById(Integer resId, EntityManager em){	
		String zoek = "SELECT r FROM Resultaat r WHERE r.resultaatId = " + resId;
		TypedQuery<Resultaat> query = em.createQuery(zoek, Resultaat.class);
		List<Resultaat> resultList = query.getResultList();
		if (resultList.isEmpty()) {
			return null;
		}
		// Zoek op ID dus maar is er maar 1 resultaat
		Resultaat resultaat = resultList.get(0);
		return resultaat;
	}
	
	public void removeResultaat(Resultaat r, Wedstrijd wd, Atleet atleet){
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("AltisBesten");
		EntityManager em = emf.createEntityManager();
		
		// verwijder resultaat bij wedstrijd
		wd.getResultaten().remove(r);
		r.setWedstrijd(null);
		// verwijder resultaat bij atleet		
		atleet.getResultaten().remove(r);
		r.setAtleet(null);
		// verwijder resultaat
		em.remove(r);
		
		// commit to database
		em.getTransaction().commit();
	}

	public List<Resultaat> getMeerkampResultaat(String naam, Long resultaatMeerkampId) {
		// System.out.println("Meerkamp: " + resultaatMeerkampId);
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("AltisBesten");
		EntityManager em = emf.createEntityManager();

		AtleetDao dDao = new AtleetDao();
		Atleet atleet = dDao.zoekAtleet(naam);
		// Haal het meerkamp resultaat op
		String zoek = "SELECT r FROM Resultaat r WHERE r.resultaatId = " + resultaatMeerkampId;
		TypedQuery<Resultaat> query = em.createQuery(zoek, Resultaat.class);
		List<Resultaat> resultList = query.getResultList();
		if (resultList.isEmpty()) {
			return null;
		}
		// Zoek op ID dus maar is er maar 1 resultaat
		Resultaat resultaat = resultList.get(0);
		// Haal de meerkamp-onderdelen op
		List<Onderdeel> meerkampOnderdelen = resultaat.getOnderdeel().getMeerkampOnderdelen();

		List<Resultaat> meerkampResultaten = new ArrayList<>();
		for (Onderdeel ond : meerkampOnderdelen) {
			// System.out.println("Onderdeel: " + ond.getKorteNaam() + " " +
			// ond.getOnderdeelId());
			String zoek2 = "SELECT DISTINCT(r) FROM Resultaat r JOIN r.wedstrijd w JOIN r.onderdeel o JOIN o.meerkamp m JOIN r.atleet d JOIN o.categorien g WHERE m.onderdeelId = "
					+ resultaat.getOnderdeel().getOnderdeelId() + " AND o.onderdeelId = " + ond.getOnderdeelId() + " AND w.wedstrijdId = " + resultaat.getWedstrijd().getWedstrijdId()
					+ " AND " + " d.naam = '" + atleet.getNaam() + "'" + " AND g.cat = 'MD'";
//			System.out.println(zoek2);
			TypedQuery<Resultaat> query2 = em.createQuery(zoek2, Resultaat.class);
			List<Resultaat> resultList2 = query2.getResultList();
			// System.out.println(resultList2.get(0));
			if(!resultList2.isEmpty()){
				meerkampResultaten.add(resultList2.get(0));	
			} else {
				// Onderdeel niet gedaan, niet gemeten, NM, of DNF, DNS of DQ
				// Voeg een leeg Resultaat toe met een streepje '-'
				Resultaat niet = new Resultaat();
				niet.setOnderdeel(ond);
				niet.setOrigineleWaarde("-");
				meerkampResultaten.add(niet);
			}
			
		}
		// System.out.println("------ " + "end" + " ---------");
		return meerkampResultaten;
	}

	public List<Resultaat> getMeerkampResultaten(String naam) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("AltisBesten");
		EntityManager em = emf.createEntityManager();
		AtleetDao dDao = new AtleetDao();
		Atleet atleet = dDao.zoekAtleet(naam);
		String zoek = "SELECT DISTINCT(r) FROM Resultaat r JOIN r.onderdeel o JOIN o.meerkampOnderdelen m JOIN r.atleet d JOIN o.categorien g WHERE m.onderdeelId IS NOT NULL AND d.naam = '"
				+ atleet.getNaam() + "' " + " AND g.cat = 'MD'";
		TypedQuery<Resultaat> query = em.createQuery(zoek, Resultaat.class);
		List<Resultaat> resultList = query.getResultList();
		if (resultList.isEmpty()) {
			return null;
		}
		List<Resultaat> res = new ArrayList<Resultaat>(resultList);
		return res;
	}

	public List<Resultaat> getDeelnemerResultaten(String naam) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("AltisBesten");
		EntityManager em = emf.createEntityManager();

		AtleetDao dDao = new AtleetDao();
		Atleet atleet = dDao.zoekAtleet(naam);

		String zoek = "SELECT r FROM Resultaat r JOIN r.atleet d WHERE d.naam = '" + atleet.getNaam() + "' ";
		TypedQuery<Resultaat> query = em.createQuery(zoek, Resultaat.class);
		List<Resultaat> resultList = query.getResultList();

		if (resultList.isEmpty()) {
			return null;
		}

		List<Resultaat> res = new ArrayList<Resultaat>(resultList);

		return res;
	}
	
	public List<Resultaat> getDeelnemerResultaten(String naam, EntityManager em) {

//		AtleetDao dDao = new AtleetDao();
//		Atleet atleet = dDao.zoekAtleet(naam);

		String zoek = "SELECT r FROM Resultaat r JOIN r.atleet d WHERE d.naam = '" + naam + "' ";
		TypedQuery<Resultaat> query = em.createQuery(zoek, Resultaat.class);
		List<Resultaat> resultList = query.getResultList();

		if (resultList.isEmpty()) {
			return null;
		}
		return  new ArrayList<Resultaat>(resultList);
	}

	public List<Resultaat> getWedstrijdResultatenByAnuId(String atletieknuId) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("AltisBesten");
		EntityManager em = emf.createEntityManager();

		String zoek = "SELECT DISTINCT(r) FROM Resultaat r JOIN r.wedstrijd w WHERE w.anuId = "
				+ new Integer(atletieknuId);
		TypedQuery<Resultaat> query = em.createQuery(zoek, Resultaat.class);
		List<Resultaat> resultList = query.getResultList();

		if (resultList.isEmpty()) {
			return null;
		}

		List<Resultaat> res = new ArrayList<Resultaat>(resultList);

		return res;
	}
	
	public List<Resultaat> getWedstrijdResultatenByWedstrijdId(String wedstrijdId) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("AltisBesten");
		EntityManager em = emf.createEntityManager();

		String zoek = "SELECT DISTINCT(r) FROM Resultaat r JOIN r.wedstrijd w WHERE w.wedstrijdId = "
				+ new Integer(wedstrijdId);
		TypedQuery<Resultaat> query = em.createQuery(zoek, Resultaat.class);
		List<Resultaat> resultList = query.getResultList();

		if (resultList.isEmpty()) {
			return null;
		}

		List<Resultaat> res = new ArrayList<Resultaat>(resultList);

		return res;
	}
	
	public List<Resultaat> getResultatenByPeriode(Date datumVanaf, Date datumTot) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("AltisBesten");
		EntityManager em = emf.createEntityManager();

		String zoek = "SELECT DISTINCT(r) FROM Resultaat r WHERE r.datum >= :dv AND r.datum <= :dt";
		TypedQuery<Resultaat> query = em.createQuery(zoek, Resultaat.class);
		query.setParameter("dv", datumVanaf, TemporalType.DATE);
		query.setParameter("dt", datumTot, TemporalType.DATE);
		List<Resultaat> resultList = query.getResultList();

		if (resultList.isEmpty()) {
			return null;
		}

		List<Resultaat> res = new ArrayList<Resultaat>(resultList);

		return res;
	}
	
	public String zoekAltisAtleten(Wedstrijd anuWedstrijd, UitslagDeelnemer uitslag,
			Map<String, Map<String, Set<String>>> chronoloog) throws ParseException {

		String altisAtleet = null;

		// Resultaten toevoegen aan de database
		for (int i = 0; i < uitslag.getOnderdelen().size(); i++) {
			String origineleWaarde = uitslag.getWaarden().get(i);
			// Bij het NSK is de vereniging niet altijd Altis
			if (!origineleWaarde.contains("--")
					&& !origineleWaarde.contains("DQ") && !origineleWaarde.contains("DNS")
					&& !origineleWaarde.contains("DNF") && !origineleWaarde.contains("NM")) {

				
				// Zoek deelnemer
				AtleetDao dDao = new AtleetDao();
				Atleet atleet = dDao.zoekAtleet(uitslag.getNaam());
				if (atleet == null) {
				} else {
					System.out.println("Altis----- " + uitslag.getNaam());
					altisAtleet = uitslag.getNaam();
				}
				
			}
		}
		return altisAtleet;
	}
	
	public void voegResultaatVerenigingToeAanWedstrijd(Wedstrijd anuWedstrijd, UitslagDeelnemer uitslag,
			Map<String, Map<String, Set<String>>> chronoloog) throws ParseException {

		/* Create EntityManagerFactory */
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("AltisBesten");

		/* Create EntityManager */
		EntityManager em = emf.createEntityManager();

		// Resultaten toevoegen aan de database
		for (int i = 0; i < uitslag.getOnderdelen().size(); i++) {
			String origineleWaarde = uitslag.getWaarden().get(i);
			if (uitslag.getVereniging().equalsIgnoreCase("Altis") && !origineleWaarde.contains("--")
					&& !origineleWaarde.contains("DQ") && !origineleWaarde.contains("DNS")
					&& !origineleWaarde.contains("DNF") && !origineleWaarde.contains("NM")) {
				/* Start transaction */
				em.getTransaction().begin();
				
				Resultaat res = new Resultaat();
				// windmeeting alleen bij enkelvoudige resultaten
				if (uitslag.getOnderdelen().size() == 1) {
					res.setWind(uitslag.getWind());
				}

				res.setWedstrijd(anuWedstrijd);
				// Recreanten Mannen - Meerkamp Subcat M45
				if (uitslag.getCat() != null && !uitslag.getCat().isEmpty()) {
					res.setCategorie(Categorie.getKorteNaam(uitslag.getCat()));
					// res.setCategorie(uitslag.getCat());
				} else { // Meisjes Junior D - Verspringen
					String[] categorieOnderdeel = uitslag.getCategorie().split("-");
					res.setCategorie(Categorie.getKorteNaam(categorieOnderdeel[0]).trim());
				}

				// onderdelen=[Ver], waarden=[2,98]
				String ond = uitslag.getOnderdelen().get(i);

				// Zoek de dag van het onderdeel als de wedstrijd over meerdere
				// dagen gaat
				String dag;
				if (anuWedstrijd.getDatumTot() != null) {
					// bij het zoeken naar de dag van het onderdeel moet je de
					// afkorting gebruiken:
					// 400m en 400m_f worden vaak op een verschillende dag
					// gelopen
					dag = getDagVanOnderdeel(chronoloog, ond, res.getCategorie());
					// het Punten totaal van de meerkamp staat niet op de
					// Chronoloog als onderdeel. Neem de laatste dag als dag.
					if (dag == null) {
						System.out.println("ONDERDEEL: " + ond + " niet gevonden in de Chronoloog!!");
						dag = anuWedstrijd.getDatumTot();
					}
				} else {
					dag = anuWedstrijd.getDatumVan();
				}
				DateFormat sorteerDatumFormatter = new SimpleDateFormat("yyyy-MM-dd");
				res.setDatum(sorteerDatumFormatter.parse(dag));
				System.out.println("DAG: " + dag);

				// Zoek het onderdeel bij de juiste categorie in de database en
				// koppel het
				OnderdeelDao dao = new OnderdeelDao();
				String onderdeelNaam = OnderdeelAnu.getOnderdeel(ond);
				// String onderdeelNaam =
				// OnderdeelAnu.getOnderdeelKorteNaam(ond);

				Onderdeel onderdeel = null;
				if (onderdeelNaam.equalsIgnoreCase("Punten totaal")) {
					onderdeel = dao.zoekMeerkamp(res.getCategorie(), uitslag.getOnderdelen());
					// soms zit er een Puntentotaal bij een enkel onderdeel
					if (onderdeel == null && uitslag.getOnderdelen().size() > 2) {
						System.out.println("maak meerkamp");
						onderdeel = dao.maakMeerkamp(res.getCategorie(), uitslag.getOnderdelen());
					} else if (onderdeel == null) {
						// met het Puntentotaal bij een enkel onderdeel doen we
						// niks
						System.out.println("Puntentotaal bij een enkel onderdeel");
						break;
					}
				} else {
					onderdeel = dao.zoekOnderdeel(onderdeelNaam, res.getCategorie());
				}
				res.setOnderdeel(onderdeel);

				// Zoek de ronde
				Ronde ronde = OnderdeelAnu.getRonde(ond);
				res.setRonde(ronde);

				uitslag.getWaarden().get(i);
				res.setOrigineleWaarde(origineleWaarde);

				NumberFormat nf = NumberFormat.getInstance(new Locale("NL"));
				// parse de waarde
				if (origineleWaarde.contains(":")) {
					// Format mm:ss,SSS
					String[] minutenEnSec = origineleWaarde.split(":");

					Number number = nf.parse(minutenEnSec[0]);
					Double min = number.doubleValue();

					Number number2 = nf.parse(minutenEnSec[1]);
					Double sec = number2.doubleValue();

					BigDecimal secondsFromMinutes = new BigDecimal(min).multiply(new BigDecimal(60));
					res.setTijd(secondsFromMinutes.add(new BigDecimal(sec)));

				} else {
					// Format tijd ss,SSS of afstand 3,54 of punten 612
					try {
						Number nummer = nf.parse(origineleWaarde);
						if (OnderdeelAnu.getMeeteenheid(ond) == MeetEenheid.TIJD) {
							res.setTijd(new BigDecimal(nummer.doubleValue()));
						} else if (OnderdeelAnu.getMeeteenheid(ond) == MeetEenheid.AFSTAND) {
							res.setAfstand(new BigDecimal(nummer.doubleValue()));
						} else {
							res.setPunten(new BigDecimal(nummer.doubleValue()));
						}
					} catch (ParseException e) {
						if (origineleWaarde.equalsIgnoreCase("quit")) {
							res.setAfstand(new BigDecimal(0));
						} else {
							System.out.println("ERRRRROOOOORRRR");
						}
					}

				}

				em.persist(res);
				// Zoek deelnemer
				AtleetDao dDao = new AtleetDao();
				Atleet atleet = dDao.zoekAtleet(uitslag.getNaam());
				if (atleet == null) {
					String[] categorieOnderdeel = uitslag.getCategorie().split("-");
					atleet = new Atleet();
					atleet.setNaam(uitslag.getNaam());
					atleet.setGeslacht(Categorie.getGeslacht((categorieOnderdeel[0])));
					atleet.setVereniging(uitslag.getVereniging());
					em.persist(atleet);
					if (!atleet.getWedstrijden().contains(anuWedstrijd)) {
						atleet.getWedstrijden().add(anuWedstrijd);
						anuWedstrijd.getAtleten().add(atleet);
					}
					em.persist(atleet);
				}
				
				
				
				
				atleet.getResultaten().add(res);
				res.setAtleet(atleet);
				
				// if (!atleet.getWedstrijden().contains(anuWedstrijd)){
				// System.out.println("+++++++++++++++++++");
				// atleet.getWedstrijden().add(anuWedstrijd);
				// }

//				if (!atleet.getWedstrijden().contains(anuWedstrijd)) {
//					atleet.getWedstrijden().add(anuWedstrijd);
//					anuWedstrijd.getAtleten().add(atleet);
//				}

				
				anuWedstrijd.getResultaten().add(res);
				em.merge(anuWedstrijd);
				
				// commit to database
				em.getTransaction().commit();
			}
		}
//		em.getTransaction().commit();
	}
	
	public void voegResultaatAtletenToeAanWedstrijd(Wedstrijd anuWedstrijd, UitslagDeelnemer uitslag,
			Map<String, Map<String, Set<String>>> chronoloog, Set<String> altisAtleten) throws ParseException {

		/* Create EntityManagerFactory */
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("AltisBesten");

		/* Create EntityManager */
		EntityManager em = emf.createEntityManager();

		// Resultaten toevoegen aan de database
		for (int i = 0; i < uitslag.getOnderdelen().size(); i++) {
			String origineleWaarde = uitslag.getWaarden().get(i);
			if (altisAtleten.contains(uitslag.getNaam()) && !origineleWaarde.contains("--")
					&& !origineleWaarde.contains("DQ") && !origineleWaarde.contains("DNS")
					&& !origineleWaarde.contains("DNF") && !origineleWaarde.contains("NM")) {
				/* Start transaction */
				em.getTransaction().begin();
				
				Resultaat res = new Resultaat();
				// windmeeting alleen bij enkelvoudige resultaten
				if (uitslag.getOnderdelen().size() == 1) {
					res.setWind(uitslag.getWind());
				}

				res.setWedstrijd(anuWedstrijd);
				// Recreanten Mannen - Meerkamp Subcat M45
				if (uitslag.getCat() != null && !uitslag.getCat().isEmpty()) {
					res.setCategorie(Categorie.getKorteNaam(uitslag.getCat()));
					// res.setCategorie(uitslag.getCat());
				} else { // Meisjes Junior D - Verspringen
					String[] categorieOnderdeel = uitslag.getCategorie().split("-");
					res.setCategorie(Categorie.getKorteNaam(categorieOnderdeel[0]).trim());
				}

				// onderdelen=[Ver], waarden=[2,98]
				String ond = uitslag.getOnderdelen().get(i);

				// Zoek de dag van het onderdeel als de wedstrijd over meerdere
				// dagen gaat
				String dag;
				if (anuWedstrijd.getDatumTot() != null) {
					// bij het zoeken naar de dag van het onderdeel moet je de
					// afkorting gebruiken:
					// 400m en 400m_f worden vaak op een verschillende dag
					// gelopen
					dag = getDagVanOnderdeel(chronoloog, ond, res.getCategorie());
					// het Punten totaal van de meerkamp staat niet op de
					// Chronoloog als onderdeel. Neem de laatste dag als dag.
					if (dag == null) {
						System.out.println("ONDERDEEL: " + ond + " niet gevonden in de Chronoloog!!");
						dag = anuWedstrijd.getDatumTot();
					}
				} else {
					dag = anuWedstrijd.getDatumVan();
				}
				DateFormat sorteerDatumFormatter = new SimpleDateFormat("yyyy-MM-dd");
				res.setDatum(sorteerDatumFormatter.parse(dag));
				System.out.println("DAG: " + dag);

				// Zoek het onderdeel bij de juiste categorie in de database en
				// koppel het
				OnderdeelDao dao = new OnderdeelDao();
				String onderdeelNaam = OnderdeelAnu.getOnderdeel(ond);
				// String onderdeelNaam =
				// OnderdeelAnu.getOnderdeelKorteNaam(ond);

				Onderdeel onderdeel = null;
				if (onderdeelNaam.equalsIgnoreCase("Punten totaal")) {
					onderdeel = dao.zoekMeerkamp(res.getCategorie(), uitslag.getOnderdelen());
					// soms zit er een Puntentotaal bij een enkel onderdeel
					if (onderdeel == null && uitslag.getOnderdelen().size() > 2) {
						System.out.println("maak meerkamp");
						onderdeel = dao.maakMeerkamp(res.getCategorie(), uitslag.getOnderdelen());
					} else if (onderdeel == null) {
						// met het Puntentotaal bij een enkel onderdeel doen we
						// niks
						System.out.println("Puntentotaal bij een enkel onderdeel");
						break;
					}
				} else {
					onderdeel = dao.zoekOnderdeel(onderdeelNaam, res.getCategorie());
				}
				res.setOnderdeel(onderdeel);

				// Zoek de ronde
				Ronde ronde = OnderdeelAnu.getRonde(ond);
				res.setRonde(ronde);

				uitslag.getWaarden().get(i);
				res.setOrigineleWaarde(origineleWaarde);

				NumberFormat nf = NumberFormat.getInstance(new Locale("NL"));
				// parse de waarde
				if (origineleWaarde.contains(":")) {
					// Format mm:ss,SSS
					String[] minutenEnSec = origineleWaarde.split(":");

					Number number = nf.parse(minutenEnSec[0]);
					Double min = number.doubleValue();

					Number number2 = nf.parse(minutenEnSec[1]);
					Double sec = number2.doubleValue();

					BigDecimal secondsFromMinutes = new BigDecimal(min).multiply(new BigDecimal(60));
					res.setTijd(secondsFromMinutes.add(new BigDecimal(sec)));

				} else {
					// Format tijd ss,SSS of afstand 3,54 of punten 612
					try {
						Number nummer = nf.parse(origineleWaarde);
						if (OnderdeelAnu.getMeeteenheid(ond) == MeetEenheid.TIJD) {
							res.setTijd(new BigDecimal(nummer.doubleValue()));
						} else if (OnderdeelAnu.getMeeteenheid(ond) == MeetEenheid.AFSTAND) {
							res.setAfstand(new BigDecimal(nummer.doubleValue()));
						} else {
							res.setPunten(new BigDecimal(nummer.doubleValue()));
						}
					} catch (ParseException e) {
						if (origineleWaarde.equalsIgnoreCase("quit")) {
							res.setAfstand(new BigDecimal(0));
						} else {
							System.out.println("ERRRRROOOOORRRR");
						}
					}

				}

				em.persist(res);
				// Zoek deelnemer
				AtleetDao dDao = new AtleetDao();
				Atleet atleet = dDao.zoekAtleet(uitslag.getNaam());
				if (atleet == null) {
					String[] categorieOnderdeel = uitslag.getCategorie().split("-");
					atleet = new Atleet();
					atleet.setNaam(uitslag.getNaam());
					atleet.setGeslacht(Categorie.getGeslacht((categorieOnderdeel[0])));
					atleet.setVereniging(uitslag.getVereniging());
					em.persist(atleet);
					if (!atleet.getWedstrijden().contains(anuWedstrijd)) {
						atleet.getWedstrijden().add(anuWedstrijd);
						anuWedstrijd.getAtleten().add(atleet);
					}
					em.persist(atleet);
				}
				
				
				
				
				atleet.getResultaten().add(res);
				res.setAtleet(atleet);
				
				// if (!atleet.getWedstrijden().contains(anuWedstrijd)){
				// System.out.println("+++++++++++++++++++");
				// atleet.getWedstrijden().add(anuWedstrijd);
				// }

//				if (!atleet.getWedstrijden().contains(anuWedstrijd)) {
//					atleet.getWedstrijden().add(anuWedstrijd);
//					anuWedstrijd.getAtleten().add(atleet);
//				}

				
				anuWedstrijd.getResultaten().add(res);
				em.merge(anuWedstrijd);
				
				// commit to database
				em.getTransaction().commit();
			}
		}
//		em.getTransaction().commit();
	}

	private String getDagVanOnderdeel(Map<String, Map<String, Set<String>>> chronoloog, String ond, String categorie) {
		System.out.println("Zoek Onderdeel: " + ond + " voor Categorie: " + categorie);
		Set<String> dagen = chronoloog.keySet();
		Iterator<String> iterator = dagen.iterator();
		while (iterator.hasNext()) {
			String dag = iterator.next();
			Map<String, Set<String>> planning = chronoloog.get(dag);
			Set<String> cats = planning.get(ond);
			if (cats != null) {
				if (cats.contains(categorie)) {
					System.out.println(
							"Onderdeel: " + ond + " voor Categorie: " + categorie + " gevonden op dag: " + dag);
					return dag;
				}
			}
		}
		return null;
	}
}
