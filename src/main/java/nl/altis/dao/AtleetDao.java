package nl.altis.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import nl.altis.domein.Atleet;

public class AtleetDao {
	public Atleet zoekAtleet(String naam){
		/* Create EntityManagerFactory */
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("AltisBesten");

		/* Create EntityManager */
		EntityManager em = emf.createEntityManager();
		
		// escape single quote with two single quotes
		if (naam.contains("'")){
			System.out.println(naam);
			naam = naam.replace("'", "''");	
		}
		
		String zoek = "SELECT d FROM Atleet d WHERE d.naam = '" + naam + "' ";
				System.out.println(zoek);
		TypedQuery<Atleet> query = em.createQuery(zoek, Atleet.class);
		List<Atleet> resultList = query.getResultList();
		
		if (resultList.isEmpty()){
			return null;
		}
		
		return resultList.get(0);
	}
	
	public Atleet zoekAtleet(String naam, EntityManager em){
		// escape single quote with two single quotes
		if (naam.contains("'")){
			System.out.println(naam);
			naam = naam.replace("'", "''");	
		}
		
		String zoek = "SELECT d FROM Atleet d WHERE d.naam = '" + naam + "' ";
				System.out.println(zoek);
		TypedQuery<Atleet> query = em.createQuery(zoek, Atleet.class);
		List<Atleet> resultList = query.getResultList();
		
		if (resultList.isEmpty()){
			return null;
		}
		
		return resultList.get(0);
	}
	
	public Atleet zoekAtleetLike(String naam, EntityManager em){
		// escape single quote with two single quotes
		if (naam.contains("'")){
			System.out.println(naam);
			naam = naam.replace("'", "''");	
		}
		
		String zoek = "SELECT d FROM Atleet d WHERE d.naam LIKE '%" + naam + "%' ";
				System.out.println(zoek);
		TypedQuery<Atleet> query = em.createQuery(zoek, Atleet.class);
		List<Atleet> resultList = query.getResultList();
		
		if (resultList.isEmpty()){
			return null;
		}
		
		return resultList.get(0);
	}
	
	public Atleet zoekAtleetById(String id, EntityManager em){		
		String zoek = "SELECT d FROM Atleet d WHERE d.atleetId = :id";
				System.out.println(zoek);
		TypedQuery<Atleet> query = em.createQuery(zoek, Atleet.class);
		query.setParameter("id",Integer.parseInt(id));
		List<Atleet> resultList = query.getResultList();
		
		if (resultList.isEmpty()){
			return null;
		}
		
		return resultList.get(0);
	}
}
