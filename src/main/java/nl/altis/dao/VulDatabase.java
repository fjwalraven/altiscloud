package nl.altis.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import nl.altis.atletiek.nu.Categorie;
import nl.altis.domein.Onderdeel;

public class VulDatabase {
	
	public static void main(String[] args) {
		VulDatabase vdb = new VulDatabase();
		vdb.vulOnderdelen();
	}

	public void vulOnderdelen() {
		/* Create EntityManagerFactory */
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("AltisBesten");

		/* Create EntityManager */
		EntityManager em = emf.createEntityManager();
		
		// nieuwe transactie
		em.getTransaction().begin();
		List<Onderdeel> onderdelen = vulOnderdelen(em);
		em.getTransaction().commit();
		// nieuwe transactie
		em.getTransaction().begin();
		vulCategorien(em);
		em.getTransaction().commit();
		
		// nieuwe transactie
		em.getTransaction().begin();
		
		vulPupillenOnderdelen(onderdelen, em);
		vulJuniorenOnderdelen(onderdelen, em);
		vulSeniorenVrouwenOnderdelen(onderdelen, em);
		vulSeniorenMannenOnderdelen(onderdelen, em);
		vulSpeciaalSportenOnderdelen(onderdelen, em);
		
		em.getTransaction().commit();
		

	}

	private void vulSpeciaalSportenOnderdelen(List<Onderdeel> onderdelen, EntityManager em) {
		     CategorieDao dao = new CategorieDao();
			nl.altis.domein.Categorie categorie = dao.zoekCategorie("JSJ");
			OnderdeelDao oDao = new OnderdeelDao();
			List<Onderdeel> algemeneOnderdelen = oDao.zoekOnderdelen("100m", "800m", "ver");
			Onderdeel kogelMD = oDao.zoekOnderdeelWaarde("kogel", 2);
			Onderdeel speerMD = oDao.zoekOnderdeelWaarde("speer", 400);
			Onderdeel vortex = oDao.zoekOnderdeel("vortex");
			categorie.getOnderdelen().addAll(algemeneOnderdelen);
			categorie.getOnderdelen().add(kogelMD);
			categorie.getOnderdelen().add(speerMD);
			categorie.getOnderdelen().add(vortex);
			for(Onderdeel o: algemeneOnderdelen){
				o.getCategorien().add(categorie);	
			}
			em.merge(categorie);
		
			categorie = dao.zoekCategorie("MSJ");
			categorie.getOnderdelen().addAll(algemeneOnderdelen);
			categorie.getOnderdelen().add(vortex);
			categorie.getOnderdelen().add(kogelMD);
			categorie.getOnderdelen().add(speerMD);
			for(Onderdeel o: algemeneOnderdelen){
				o.getCategorien().add(categorie);	
			}
			em.merge(categorie);
			
			categorie = dao.zoekCategorie("MSP");
			categorie.getOnderdelen().addAll(algemeneOnderdelen);
			categorie.getOnderdelen().add(vortex);
			categorie.getOnderdelen().add(kogelMD);
			categorie.getOnderdelen().add(speerMD);
			for(Onderdeel o: algemeneOnderdelen){
				o.getCategorien().add(categorie);	
			}
			em.merge(categorie);
			
			categorie = dao.zoekCategorie("JSP");
			categorie.getOnderdelen().addAll(algemeneOnderdelen);
			categorie.getOnderdelen().add(vortex);
			categorie.getOnderdelen().add(kogelMD);
			categorie.getOnderdelen().add(speerMD);
			for(Onderdeel o: algemeneOnderdelen){
				o.getCategorien().add(categorie);	
			}
			em.merge(categorie);
	}

	private void vulMeerkampJuniorenC(EntityManager em) {
     	CategorieDao catDao = new CategorieDao();
		nl.altis.domein.Categorie catJC = catDao.zoekCategorie("JC");
		Onderdeel meerkamp = new Onderdeel();
		
		meerkamp.setKorteNaam("meerkamp");
		meerkamp.setLangeNaam("8-kamp junioren C");
		meerkamp.getCategorien().add(catJC);
		em.persist(meerkamp);
		catJC.getOnderdelen().add(meerkamp);
		em.merge(catJC);
		em.getTransaction().commit();
		
		em.getTransaction().begin();
		OnderdeelDao dao = new OnderdeelDao();
		Onderdeel mk = dao.zoekOnderdeel("meerkamp", "JC");
		
		Onderdeel kogel = dao.zoekOnderdeel("kogel", "JC");
		kogel.getMeerkamp().add(mk);
		Onderdeel _100M = dao.zoekOnderdeel("100m", "JC");
		 _100M.getMeerkamp().add(mk);
		 Onderdeel _100MH = dao.zoekOnderdeel("100mH", "JC");
		 _100MH.getMeerkamp().add(mk);
		Onderdeel _1000M = dao.zoekOnderdeel("1000m", "JC");
		_1000M.getMeerkamp().add(mk);
		Onderdeel speer = dao.zoekOnderdeel("speer", "JC");
		speer.getMeerkamp().add(mk);
		Onderdeel ver = dao.zoekOnderdeel("ver", "JC");
		ver.getMeerkamp().add(mk);
		Onderdeel hoog = dao.zoekOnderdeel("hoog", "JC");
		hoog.getMeerkamp().add(mk);
		Onderdeel discus = dao.zoekOnderdeel("discus", "JC");
		discus.getMeerkamp().add(mk);
		
		mk.getMeerkampOnderdelen().add(kogel);
		mk.getMeerkampOnderdelen().add(_100M);
		mk.getMeerkampOnderdelen().add(_100MH);
		mk.getMeerkampOnderdelen().add(_1000M);
		mk.getMeerkampOnderdelen().add(speer);
		mk.getMeerkampOnderdelen().add(ver);
		mk.getMeerkampOnderdelen().add(hoog);
		mk.getMeerkampOnderdelen().add(discus);
		
		em.merge(kogel);
		em.merge(_100M);
		em.merge(_100MH);
		em.merge(_1000M);
		em.merge(speer);
		em.merge(ver);
		em.merge(hoog);
		em.merge(discus);
		em.merge(mk);
				
	}

	private void vulSeniorenMannenOnderdelen(List<Onderdeel> onderdelen, EntityManager em) {
		CategorieDao dao = new CategorieDao();
		OnderdeelDao oDao = new OnderdeelDao();
		List<Onderdeel> algemeneOnderdelen = oDao.zoekOnderdelen("50m", "60m", "100m", "150m", "200m", "300m", "400m", "600m", "800m", "1000m", "1500m", "1Mijl", "2000m", "3000m","5000m", "10000m", "coop60", "coop6", "coop", "ver", "hoog", "hss", "4 x 50m", "4 x 60m", "4 x 100m", "4 x 200m", "4 x 400m");
		nl.altis.domein.Categorie categorie = dao.zoekCategorie("Senioren mannen");
		categorie.getOnderdelen().addAll(algemeneOnderdelen);
		Onderdeel _50mH = oDao.zoekOnderdeelExtraWaarde("50mH", 106.7);
		Onderdeel _60mH = oDao.zoekOnderdeelExtraWaarde("60mH", 106.7);
		Onderdeel _110mH = oDao.zoekOnderdeelExtraWaarde("110mH", 106.7);
		Onderdeel _200mH = oDao.zoekOnderdeelExtraWaarde("200mH", 76.2);
		Onderdeel _400mH = oDao.zoekOnderdeelExtraWaarde("400mH", 91.4);
		Onderdeel kogel = oDao.zoekOnderdeelWaarde("kogel", 7.26);
		Onderdeel discus = oDao.zoekOnderdeelWaarde("discus", 2);
		Onderdeel speer = oDao.zoekOnderdeelWaarde("speer", 800);
		Onderdeel kogelsl = oDao.zoekOnderdeelWaarde("kogelsl", 7.26);
		Onderdeel _3000m_SC = oDao.zoekOnderdeel("3000m SC");
		Onderdeel polshoog = oDao.zoekOnderdeel("polshoog");
		categorie.getOnderdelen().add(_50mH);
		categorie.getOnderdelen().add(_60mH);
		categorie.getOnderdelen().add(_110mH);
		categorie.getOnderdelen().add(_200mH);
		categorie.getOnderdelen().add(_400mH);
		categorie.getOnderdelen().add(kogel);
		categorie.getOnderdelen().add(discus);
		categorie.getOnderdelen().add(speer);
		categorie.getOnderdelen().add(kogelsl);
		categorie.getOnderdelen().add(_3000m_SC);
		categorie.getOnderdelen().add(polshoog);
		for(Onderdeel o: algemeneOnderdelen){
			o.getCategorien().add(categorie);	
		}
		em.merge(categorie);
		
		String[] cats = new String[]{"Mmas", "M35", "M40", "M45"};
		for (String catV: cats){
			nl.altis.domein.Categorie catM35 = dao.zoekCategorie(catV);
			Onderdeel _50mHM35 = oDao.zoekOnderdeelExtraWaarde("50mH", 99.1);
			Onderdeel _60mHM35 = oDao.zoekOnderdeelExtraWaarde("60mH", 99.1);
			Onderdeel _100mHM35 = oDao.zoekOnderdeelExtraWaardeToel("100mH", 91.4, "8.5");
			Onderdeel _110mHM35 = oDao.zoekOnderdeelExtraWaarde("110mH", 99.1);
			Onderdeel _400mHM35 = oDao.zoekOnderdeelExtraWaarde("400mH", 91.4);
			Onderdeel gewichtwerpen = oDao.zoekOnderdeelWaarde("gewicht", 15.88);
			catM35.getOnderdelen().add(gewichtwerpen);
			catM35.getOnderdelen().add(_50mHM35);
			catM35.getOnderdelen().add(_60mHM35);
			catM35.getOnderdelen().add(_100mHM35);
			catM35.getOnderdelen().add(_110mHM35);
			catM35.getOnderdelen().add(_200mH);
			catM35.getOnderdelen().add(_400mHM35);
			catM35.getOnderdelen().add(kogel);
			catM35.getOnderdelen().addAll(algemeneOnderdelen);
			catM35.getOnderdelen().add(discus);
			catM35.getOnderdelen().add(speer);
			catM35.getOnderdelen().add(kogelsl);
			catM35.getOnderdelen().add(_3000m_SC);
			catM35.getOnderdelen().add(polshoog);
			for(Onderdeel o: algemeneOnderdelen){
				o.getCategorien().add(catM35);	
			}
			em.merge(catM35);
		}	
		String[] catsM50 = new String[]{"M50", "M55"};
		for (String catV: catsM50){
			nl.altis.domein.Categorie cats50 = dao.zoekCategorie(catV);
			Onderdeel _50mHM35 = oDao.zoekOnderdeelExtraWaardeToel("50mH", 91.4, "9.14");
			Onderdeel _60mHM35 = oDao.zoekOnderdeelExtraWaardeToel("60mH", 91.4, "9.14");
			Onderdeel _100mHM35 = oDao.zoekOnderdeelExtraWaardeToel("100mH", 91.4, "8.5");
			Onderdeel _110mHM35 = oDao.zoekOnderdeelExtraWaarde("110mH", 91.4);
			Onderdeel _400mHM35 = oDao.zoekOnderdeelExtraWaarde("400mH", 83.8);
			Onderdeel kogelM50 = oDao.zoekOnderdeelWaarde("kogel", 6);
			Onderdeel discusM50 = oDao.zoekOnderdeelWaarde("discus", 1.5);
			Onderdeel speerM50 = oDao.zoekOnderdeelWaarde("speer", 700);
			Onderdeel kogelslM50 = oDao.zoekOnderdeelWaarde("kogelsl", 6);
			Onderdeel gewichtwerpen = oDao.zoekOnderdeelWaarde("gewicht", 11.34);
			cats50.getOnderdelen().add(gewichtwerpen);
			cats50.getOnderdelen().add(_50mHM35);
			cats50.getOnderdelen().add(_60mHM35);
			cats50.getOnderdelen().add(_100mHM35);
			cats50.getOnderdelen().add(_110mHM35);
			cats50.getOnderdelen().add(_400mHM35);
			cats50.getOnderdelen().addAll(algemeneOnderdelen);
			cats50.getOnderdelen().add(kogelM50);
			cats50.getOnderdelen().add(discusM50);
			cats50.getOnderdelen().add(speerM50);
			cats50.getOnderdelen().add(kogelslM50);
			cats50.getOnderdelen().add(_3000m_SC);
			cats50.getOnderdelen().add(polshoog);
			for(Onderdeel o: algemeneOnderdelen){
				o.getCategorien().add(cats50);	
			}
			em.merge(cats50);
		}
		String[] catsM60 = new String[]{"M60", "M65"};
		for (String catV: catsM60){
			nl.altis.domein.Categorie cats60 = dao.zoekCategorie(catV);
			Onderdeel _50mHM60 = oDao.zoekOnderdeelExtraWaardeToel("50mH", 83.8, "8");
			Onderdeel _60mHM60 = oDao.zoekOnderdeelExtraWaardeToel("60mH", 83.8, "8");
			Onderdeel _100mHM60 = oDao.zoekOnderdeelExtraWaardeToel("100mH", 83.8, "8");
			Onderdeel _300mHM60 = oDao.zoekOnderdeelExtraWaarde("300mH", 76.2);
			Onderdeel kogelM60 = oDao.zoekOnderdeelWaarde("kogel", 5);
			Onderdeel discusM60 = oDao.zoekOnderdeelWaarde("discus", 1);
			Onderdeel speerM60 = oDao.zoekOnderdeelWaarde("speer", 600);
			Onderdeel kogelslM60 = oDao.zoekOnderdeelWaarde("kogelsl", 5);
			Onderdeel gewichtwerpen = oDao.zoekOnderdeelWaarde("gewicht", 9.08);
			cats60.getOnderdelen().add(gewichtwerpen);
			cats60.getOnderdelen().add(_50mHM60);
			cats60.getOnderdelen().add(_60mHM60);
			cats60.getOnderdelen().add(_100mHM60);
			cats60.getOnderdelen().add(_300mHM60);
			cats60.getOnderdelen().add(kogel);
			cats60.getOnderdelen().addAll(algemeneOnderdelen);
			cats60.getOnderdelen().add(kogelM60);
			cats60.getOnderdelen().add(discusM60);
			cats60.getOnderdelen().add(speerM60);
			cats60.getOnderdelen().add(kogelslM60);
			cats60.getOnderdelen().add(_3000m_SC);
			cats60.getOnderdelen().add(polshoog);
			for(Onderdeel o: algemeneOnderdelen){
				o.getCategorien().add(cats60);	
			}
			em.merge(cats60);
		}
		String[] catsM70 = new String[]{"M70", "M75"};
		for (String catV: catsM70){
			nl.altis.domein.Categorie cats70 = dao.zoekCategorie(catV);
			Onderdeel _50mHM70 = oDao.zoekOnderdeelExtraWaardeToel("50mH", 76.2, "7");
			Onderdeel _60mHM70 = oDao.zoekOnderdeelExtraWaardeToel("60mH", 76.2, "7");
			Onderdeel _80mHM70 = oDao.zoekOnderdeelExtraWaardeToel("80mH", 76.2, "7");
			Onderdeel _300mHM70 = oDao.zoekOnderdeelExtraWaarde("300mH", 68.6);
			Onderdeel kogelM70 = oDao.zoekOnderdeelWaarde("kogel", 4);
			Onderdeel discusM70 = oDao.zoekOnderdeelWaarde("discus", 1);
			Onderdeel speerM70 = oDao.zoekOnderdeelWaarde("speer", 500);
			Onderdeel kogelslM70 = oDao.zoekOnderdeelWaarde("kogelsl", 4);
			Onderdeel gewichtwerpen = oDao.zoekOnderdeelWaarde("gewicht", 7.26);
			cats70.getOnderdelen().add(gewichtwerpen);
			cats70.getOnderdelen().add(_50mHM70);
			cats70.getOnderdelen().add(_60mHM70);
			cats70.getOnderdelen().add(_80mHM70);
			cats70.getOnderdelen().add(_300mHM70);
			cats70.getOnderdelen().addAll(algemeneOnderdelen);
			cats70.getOnderdelen().add(kogelM70);
			cats70.getOnderdelen().add(discusM70);
			cats70.getOnderdelen().add(speerM70);
			cats70.getOnderdelen().add(kogelslM70);
			cats70.getOnderdelen().add(_3000m_SC);
			cats70.getOnderdelen().add(polshoog);
			for(Onderdeel o: algemeneOnderdelen){
				o.getCategorien().add(cats70);	
			}
			em.merge(cats70);
		}
		String[] catsM80 = new String[]{"M80"};
		for (String catV: catsM80){
			nl.altis.domein.Categorie cats80 = dao.zoekCategorie(catV);
			Onderdeel _50mHM70 = oDao.zoekOnderdeelExtraWaardeToel("50mH", 68.6, "7");
			Onderdeel _60mHM70 = oDao.zoekOnderdeelExtraWaardeToel("60mH", 68.6, "7");
			Onderdeel _80mHM70 = oDao.zoekOnderdeelExtraWaardeToel("80mH", 68.6, "7");
			Onderdeel _200mHM70 = oDao.zoekOnderdeelExtraWaarde("200mH", 68.6);
			Onderdeel kogelM70 = oDao.zoekOnderdeelWaarde("kogel", 3);
			Onderdeel discusM70 = oDao.zoekOnderdeelWaarde("discus", 1);
			Onderdeel speerM70 = oDao.zoekOnderdeelWaarde("speer", 400);
			Onderdeel kogelslM70 = oDao.zoekOnderdeelWaarde("kogelsl", 3);
			Onderdeel gewichtwerpen = oDao.zoekOnderdeelWaarde("gewicht", 5.45);
			cats80.getOnderdelen().add(gewichtwerpen);
			cats80.getOnderdelen().add(_50mHM70);
			cats80.getOnderdelen().add(_60mHM70);
			cats80.getOnderdelen().add(_80mHM70);
			cats80.getOnderdelen().add(_200mHM70);
			cats80.getOnderdelen().addAll(algemeneOnderdelen);
			cats80.getOnderdelen().add(kogelM70);
			cats80.getOnderdelen().add(discusM70);
			cats80.getOnderdelen().add(speerM70);
			cats80.getOnderdelen().add(kogelslM70);
			cats80.getOnderdelen().add(_3000m_SC);
			cats80.getOnderdelen().add(polshoog);
			for(Onderdeel o: algemeneOnderdelen){
				o.getCategorien().add(cats80);	
			}
			em.merge(cats80);
		}
	}

	private void vulSeniorenVrouwenOnderdelen(List<Onderdeel> onderdelen, EntityManager em) {
		CategorieDao dao = new CategorieDao();
		String[] cats = new String[]{"Senioren vrouwen", "Vmas","V35", "V40", "V45"};
		OnderdeelDao oDao = new OnderdeelDao();
		List<Onderdeel> algemeneOnderdelen = oDao.zoekOnderdelen("50m", "60m", "100m", "150m", "200m", "300m", "400m", "600m", "800m", "1000m", "1500m", "1Mijl", "coop60", "coop6", "coop", "2000m", "3000m","5000m", "10000m","ver", "hoog", "hss", "4 x 50m", "4 x 60m", "4 x 100m", "4 x 200m", "4 x 400m");
		for (String catV: cats){
			nl.altis.domein.Categorie categorie = dao.zoekCategorie(catV);
			Onderdeel _2000m_SC = oDao.zoekOnderdeel("2000m SC");
			Onderdeel _3000m_SC = oDao.zoekOnderdeel("3000m SC");
			Onderdeel discusMC = oDao.zoekOnderdeelWaarde("discus", 1);
			Onderdeel kogelsl_C = oDao.zoekOnderdeelWaarde("kogelsl", 4);
			Onderdeel kogelMA = oDao.zoekOnderdeelWaarde("kogel", 4);
			Onderdeel speerMA = oDao.zoekOnderdeelWaarde("speer", 600);
			Onderdeel polshoog = oDao.zoekOnderdeel("polshoog");	
			Onderdeel gewichtwerpen = oDao.zoekOnderdeelWaarde("gewicht", 9.08);
			categorie.getOnderdelen().add(gewichtwerpen);
			categorie.getOnderdelen().addAll(algemeneOnderdelen);
			categorie.getOnderdelen().add(_2000m_SC);
			categorie.getOnderdelen().add(_3000m_SC);
			categorie.getOnderdelen().add(discusMC);
			categorie.getOnderdelen().add(kogelsl_C);
			categorie.getOnderdelen().add(kogelMA);
			categorie.getOnderdelen().add(speerMA);
			categorie.getOnderdelen().add(polshoog);
			for(Onderdeel o: algemeneOnderdelen){
				o.getCategorien().add(categorie);	
			}
			if (catV.equalsIgnoreCase("Senioren vrouwen") || catV.equalsIgnoreCase("V35")){
				Onderdeel _50mH_C = oDao.zoekOnderdeelExtraWaardeToel("50mH", 83.8, "8.5");
				Onderdeel _60mH_C = oDao.zoekOnderdeelExtraWaardeToel("60mH", 83.8, "8.5");
				Onderdeel _100mH_C = oDao.zoekOnderdeelExtraWaardeToel("100mH", 83.8, "8.5");
				Onderdeel _200mH = oDao.zoekOnderdeelExtraWaarde("200mH", 76.2);
				Onderdeel _400mH = oDao.zoekOnderdeelExtraWaarde("400mH", 76.2);
				categorie.getOnderdelen().add(_50mH_C);
				categorie.getOnderdelen().add(_60mH_C);
				categorie.getOnderdelen().add(_100mH_C);
				categorie.getOnderdelen().add(_400mH);
				categorie.getOnderdelen().add(_200mH);
			} else if (catV.equalsIgnoreCase("V40") || catV.equalsIgnoreCase("V45")){
				Onderdeel _80mH = oDao.zoekOnderdeelExtraWaardeToel("80mH", 76.2, "8");
				Onderdeel _400mH = oDao.zoekOnderdeelExtraWaarde("400mH", 76.2);
				categorie.getOnderdelen().add(_80mH);
				categorie.getOnderdelen().add(_400mH);
			}
			em.merge(categorie);
		}
		String[] cats50 = new String[]{"V50", "V55"};
		for (String catV: cats50){
			nl.altis.domein.Categorie categorie = dao.zoekCategorie(catV);
			Onderdeel _50mH = oDao.zoekOnderdeelExtraWaardeToel("50mH", 76.2, "7");
			Onderdeel _60mH = oDao.zoekOnderdeelExtraWaardeToel("60mH", 76.2, "7");
			Onderdeel _80mH = oDao.zoekOnderdeelExtraWaardeToel("80mH", 76.2, "7");
			Onderdeel _100mH_C = oDao.zoekOnderdeelExtraWaarde("100mH", 76.2);
			Onderdeel _300mH = oDao.zoekOnderdeelExtraWaarde("300mH", 76.2);
			Onderdeel _2000m_SC = oDao.zoekOnderdeel("2000m SC");
			categorie.getOnderdelen().add(_50mH);
			categorie.getOnderdelen().add(_60mH);
			categorie.getOnderdelen().add(_80mH);
			categorie.getOnderdelen().add(_100mH_C);
			categorie.getOnderdelen().add(_300mH);
			categorie.getOnderdelen().add(_2000m_SC);
			Onderdeel discus = oDao.zoekOnderdeelWaarde("discus", 1);
			Onderdeel kogelsl = oDao.zoekOnderdeelWaarde("kogelsl", 3);
			Onderdeel kogel = oDao.zoekOnderdeelWaarde("kogel", 3);
			Onderdeel speer = oDao.zoekOnderdeelWaarde("speer", 500);
			Onderdeel gewichtwerpen = oDao.zoekOnderdeelWaarde("gewicht", 7.26);
			categorie.getOnderdelen().add(gewichtwerpen);
			categorie.getOnderdelen().add(discus);
			categorie.getOnderdelen().add(kogelsl);
			categorie.getOnderdelen().add(kogel);
			categorie.getOnderdelen().add(speer);
			categorie.getOnderdelen().addAll(algemeneOnderdelen);
			em.merge(categorie);
		}		
		String[] cats60 = new String[]{"V60", "V65", "V70", "V75", "V80"};
		for (String catV: cats60){
			nl.altis.domein.Categorie categorie = dao.zoekCategorie(catV);
			Onderdeel _50mH = oDao.zoekOnderdeelExtraWaardeToel("50mH", 76.2, "7");
			Onderdeel _60mH = oDao.zoekOnderdeelExtraWaardeToel("60mH", 76.2, "7");
			Onderdeel _80mH = oDao.zoekOnderdeelExtraWaardeToel("80mH", 68.6, "7");
			Onderdeel _100mH_C = oDao.zoekOnderdeelExtraWaarde("100mH", 76.2);
			Onderdeel _300mH = oDao.zoekOnderdeelExtraWaarde("300mH", 68.6);
			Onderdeel _2000m_SC = oDao.zoekOnderdeel("2000m SC");
			categorie.getOnderdelen().add(_50mH);
			categorie.getOnderdelen().add(_60mH);
			categorie.getOnderdelen().add(_80mH);
			categorie.getOnderdelen().add(_100mH_C);
			categorie.getOnderdelen().add(_300mH);
			categorie.getOnderdelen().add(_2000m_SC);
			Onderdeel discus = oDao.zoekOnderdeelWaarde("discus", 1);
			Onderdeel kogelsl = oDao.zoekOnderdeelWaarde("kogelsl", 3);
			Onderdeel kogel = oDao.zoekOnderdeelWaarde("kogel", 3);
			Onderdeel speer = oDao.zoekOnderdeelWaarde("speer", 500);
			Onderdeel gewichtwerpen = oDao.zoekOnderdeelWaarde("gewicht", 5.45);
			categorie.getOnderdelen().add(gewichtwerpen);
			categorie.getOnderdelen().add(discus);
			categorie.getOnderdelen().add(kogelsl);
			categorie.getOnderdelen().add(kogel);
			categorie.getOnderdelen().add(speer);
			categorie.getOnderdelen().addAll(algemeneOnderdelen);
			em.merge(categorie);
		}
	}

	private void vulJuniorenOnderdelen(List<Onderdeel> onderdelen, EntityManager em) {
        CategorieDao dao = new CategorieDao();
		nl.altis.domein.Categorie categorie = dao.zoekCategorie("MD");
		OnderdeelDao oDao = new OnderdeelDao();
		List<Onderdeel> algemeneOnderdelen = oDao.zoekOnderdelen("50m", "60m", "80m", "150m", "300m", "600m", "800m", "1000m", "2000m","3000m", "3200m", "1Mijl", "1500m", "ver", "hoog", "hss", "4 x 50m", "4 x 60m");
		Onderdeel _50mH = oDao.zoekOnderdeelExtraWaardeToel("50mH", 76.2, "8");
		Onderdeel _60mH = oDao.zoekOnderdeelExtraWaardeToel("60mH", 76.2, "8");
		Onderdeel _200mH = oDao.zoekOnderdeelExtraWaarde("200mH", 76.2);
		Onderdeel _300mH = oDao.zoekOnderdeelExtraWaarde("300mH", 76.2);
		Onderdeel _1000m_SC = oDao.zoekOnderdeel("1000m SC");
		Onderdeel kogelMD = oDao.zoekOnderdeelWaarde("kogel", 2);
		Onderdeel discusMD = oDao.zoekOnderdeelWaarde("discus", 0.75);
		Onderdeel speerMD = oDao.zoekOnderdeelWaarde("speer", 400);
		Onderdeel _100m = oDao.zoekOnderdeel("100m");
		categorie.getOnderdelen().add(_100m);
		Onderdeel _200m = oDao.zoekOnderdeel("200m");
		categorie.getOnderdelen().add(_200m);
		Onderdeel _400m = oDao.zoekOnderdeel("400m");
		categorie.getOnderdelen().add(_400m);
		categorie.getOnderdelen().addAll(algemeneOnderdelen);
		categorie.getOnderdelen().add(_50mH);
		categorie.getOnderdelen().add(_60mH);
		categorie.getOnderdelen().add(_200mH);
		categorie.getOnderdelen().add(_300mH);
		categorie.getOnderdelen().add(_1000m_SC);
		categorie.getOnderdelen().add(kogelMD);
		categorie.getOnderdelen().add(discusMD);
		categorie.getOnderdelen().add(speerMD);
		for(Onderdeel o: algemeneOnderdelen){
			o.getCategorien().add(categorie);	
		}
		em.merge(categorie);
		
		categorie = dao.zoekCategorie("MC");
		categorie.getOnderdelen().addAll(algemeneOnderdelen);
		categorie.getOnderdelen().add(_50mH);
		categorie.getOnderdelen().add(_60mH);
		categorie.getOnderdelen().add(_100m);
		categorie.getOnderdelen().add(_200m);
		categorie.getOnderdelen().add(_400m);
		Onderdeel kogelMC = oDao.zoekOnderdeelWaarde("kogel", 3);
		Onderdeel discusMC = oDao.zoekOnderdeelWaarde("discus", 1);
		Onderdeel speerMC = oDao.zoekOnderdeelWaarde("speer", 500);
		Onderdeel _1500m_SC = oDao.zoekOnderdeel("1500m SC");
		Onderdeel _80mH = oDao.zoekOnderdeelExtraWaardeToel("80mH", 76.2, "8");
		Onderdeel _4x80 = oDao.zoekOnderdeel("4 x 80m");
		Onderdeel _4x800 = oDao.zoekOnderdeel("4 x 800m");
		Onderdeel polshoog = oDao.zoekOnderdeel("polshoog");
		categorie.getOnderdelen().add(kogelMC);
		categorie.getOnderdelen().add(discusMC);
		categorie.getOnderdelen().add(speerMC);
		categorie.getOnderdelen().add(_1500m_SC);
		categorie.getOnderdelen().add(_80mH);
		categorie.getOnderdelen().add(_200mH);
		categorie.getOnderdelen().add(_300mH);
		categorie.getOnderdelen().add(_4x80);
		categorie.getOnderdelen().add(_4x800);
		categorie.getOnderdelen().add(polshoog);
		for(Onderdeel o: algemeneOnderdelen){
			o.getCategorien().add(categorie);	
		}
		em.merge(categorie);
		
		categorie = dao.zoekCategorie("MB");
		categorie.getOnderdelen().addAll(algemeneOnderdelen);
		Onderdeel _50mH_B = oDao.zoekOnderdeelExtraWaardeToel("50mH", 76.2, "8.5");
		Onderdeel _60mH_B = oDao.zoekOnderdeelExtraWaardeToel("60mH", 76.2, "8.5");
		categorie.getOnderdelen().add(_50mH_B);
		categorie.getOnderdelen().add(_60mH_B);
		List<Onderdeel> algemeneBOnderdelen = oDao.zoekOnderdelen("100m", "200m", "400m", "4 x 100m", "4 x 200m", "4 x 400m");
		Onderdeel _100mH = oDao.zoekOnderdeelExtraWaardeToel("100mH", 76.2, "8.5");
		Onderdeel _400mH = oDao.zoekOnderdeelExtraWaarde("400mH", 76.2);	
		Onderdeel _2000m_SC = oDao.zoekOnderdeel("2000m SC");
		categorie.getOnderdelen().addAll(algemeneBOnderdelen);
		categorie.getOnderdelen().add(kogelMC);
		categorie.getOnderdelen().add(discusMC);
		categorie.getOnderdelen().add(speerMC);
		categorie.getOnderdelen().add(_100mH);
		categorie.getOnderdelen().add(_200mH);
		categorie.getOnderdelen().add(_300mH);
		categorie.getOnderdelen().add(_400mH);
		categorie.getOnderdelen().add(_2000m_SC);
		categorie.getOnderdelen().add(_4x800);
		categorie.getOnderdelen().add(polshoog);
		for(Onderdeel o: algemeneOnderdelen){
			o.getCategorien().add(categorie);	
		}
		for(Onderdeel o: algemeneBOnderdelen){
			o.getCategorien().add(categorie);	
		}
		em.merge(categorie);
		
		categorie = dao.zoekCategorie("MA");
		categorie.getOnderdelen().addAll(algemeneOnderdelen);
		categorie.getOnderdelen().addAll(algemeneBOnderdelen);
		Onderdeel _50mH_A = oDao.zoekOnderdeelExtraWaardeToel("50mH", 83.8, "8.5");
		Onderdeel _60mH_A = oDao.zoekOnderdeelExtraWaardeToel("60mH", 83.8, "8.5");
		Onderdeel _100mH_A = oDao.zoekOnderdeelExtraWaardeToel("100mH", 83.8, "8.5");
		categorie.getOnderdelen().add(_50mH_A);
		categorie.getOnderdelen().add(_60mH_A);
		categorie.getOnderdelen().add(_100mH_A);
		categorie.getOnderdelen().add(discusMC);
		List<Onderdeel> algemeneAOnderdelen = oDao.zoekOnderdelen("5000m", "10000m", "2000m", "3000m SC");
		Onderdeel kogelMA = oDao.zoekOnderdeelWaarde("kogel", 4);
		Onderdeel speerMA = oDao.zoekOnderdeelWaarde("speer", 600);
		categorie.getOnderdelen().add(kogelMA);	
		categorie.getOnderdelen().add(speerMA);
		categorie.getOnderdelen().add(_100mH);
		categorie.getOnderdelen().add(_200mH);
		categorie.getOnderdelen().add(_400mH);
		categorie.getOnderdelen().add(_4x800);
		categorie.getOnderdelen().add(polshoog);
		for(Onderdeel o: algemeneOnderdelen){
			o.getCategorien().add(categorie);	
		}
		for(Onderdeel o: algemeneBOnderdelen){
			o.getCategorien().add(categorie);	
		}
		for(Onderdeel o: algemeneAOnderdelen){
			o.getCategorien().add(categorie);	
		}
		em.merge(categorie);
		
		categorie = dao.zoekCategorie("JD");
		categorie.getOnderdelen().addAll(algemeneOnderdelen);
		categorie.getOnderdelen().add(_50mH);
		categorie.getOnderdelen().add(_60mH);
		categorie.getOnderdelen().add(_80mH);
		categorie.getOnderdelen().add(_200mH);
		categorie.getOnderdelen().add(_1000m_SC);
		categorie.getOnderdelen().add(speerMD);
		categorie.getOnderdelen().add(kogelMC);
		categorie.getOnderdelen().add(discusMC);
		categorie.getOnderdelen().add(polshoog);
		Onderdeel kogelsl = oDao.zoekOnderdeelWaarde("kogelsl", 3);
		categorie.getOnderdelen().add(kogelsl);
		categorie.getOnderdelen().add(_100m);
		categorie.getOnderdelen().add(_200m);
		categorie.getOnderdelen().add(_400m);
		categorie.getOnderdelen().add(_4x80);
		for(Onderdeel o: algemeneOnderdelen){
			o.getCategorien().add(categorie);	
		}
		em.merge(categorie);
		
		categorie = dao.zoekCategorie("JC");
		categorie.getOnderdelen().addAll(algemeneOnderdelen);
		categorie.getOnderdelen().addAll(algemeneBOnderdelen);
		categorie.getOnderdelen().add(_300mH);
		Onderdeel _100mH_C = oDao.zoekOnderdeelExtraWaarde("100mH", 83.8);
		Onderdeel kogelsl_C = oDao.zoekOnderdeelWaarde("kogelsl", 4);
		Onderdeel kogel_C = oDao.zoekOnderdeelWaarde("kogel", 4);
		categorie.getOnderdelen().add(_50mH_A);
		categorie.getOnderdelen().add(_60mH_A);
		categorie.getOnderdelen().add(_100mH_C);
		categorie.getOnderdelen().add(_200mH);
		categorie.getOnderdelen().add(kogel_C);
		categorie.getOnderdelen().add(kogelsl_C);
		categorie.getOnderdelen().add(discusMC);
		categorie.getOnderdelen().add(speerMA);
		categorie.getOnderdelen().add(_1500m_SC);
		categorie.getOnderdelen().add(polshoog);
		for(Onderdeel o: algemeneOnderdelen){
			o.getCategorien().add(categorie);	
		}
		for(Onderdeel o: algemeneBOnderdelen){
			o.getCategorien().add(categorie);	
		}
		em.merge(categorie);
		
		categorie = dao.zoekCategorie("JB");
		categorie.getOnderdelen().addAll(algemeneOnderdelen);
		categorie.getOnderdelen().addAll(algemeneBOnderdelen);
		categorie.getOnderdelen().addAll(algemeneAOnderdelen);
		Onderdeel _50mH_JB = oDao.zoekOnderdeelExtraWaarde("50mH", 91.4);
		Onderdeel _60mH_JB = oDao.zoekOnderdeelExtraWaarde("60mH", 91.4);
		Onderdeel _110mH = oDao.zoekOnderdeelExtraWaarde("110mH", 91.4);
		Onderdeel _300mH_B = oDao.zoekOnderdeelExtraWaarde("300mH", 91.4);
		Onderdeel _400mH_B = oDao.zoekOnderdeelExtraWaarde("400mH", 83.8);
		categorie.getOnderdelen().add(_50mH_JB);
		categorie.getOnderdelen().add(_60mH_JB);
		categorie.getOnderdelen().add(_110mH);
		categorie.getOnderdelen().add(_200mH);
		categorie.getOnderdelen().add(_300mH_B);
		categorie.getOnderdelen().add(_400mH_B);
		categorie.getOnderdelen().add(_2000m_SC);
		Onderdeel kogelJB = oDao.zoekOnderdeelWaarde("kogel", 5);
		Onderdeel discusJB = oDao.zoekOnderdeelWaarde("discus", 1.5);
		Onderdeel speerJB = oDao.zoekOnderdeelWaarde("speer", 700);
		Onderdeel kogelsl_B = oDao.zoekOnderdeelWaarde("kogelsl", 5);
		categorie.getOnderdelen().add(kogelJB);
		categorie.getOnderdelen().add(discusJB);
		categorie.getOnderdelen().add(speerJB);
		categorie.getOnderdelen().add(kogelsl_B);
		categorie.getOnderdelen().add(polshoog);
		for(Onderdeel o: algemeneOnderdelen){
			o.getCategorien().add(categorie);	
		}
		for(Onderdeel o: algemeneBOnderdelen){
			o.getCategorien().add(categorie);	
		}
		for(Onderdeel o: algemeneAOnderdelen){
			o.getCategorien().add(categorie);	
		}
		em.merge(categorie);
		
		categorie = dao.zoekCategorie("JA");
		categorie.getOnderdelen().addAll(algemeneOnderdelen);
		categorie.getOnderdelen().addAll(algemeneBOnderdelen);
		categorie.getOnderdelen().addAll(algemeneAOnderdelen);
		categorie.getOnderdelen().add(_2000m_SC);
		Onderdeel _50mH_JA = oDao.zoekOnderdeelExtraWaarde("50mH", 99.1);
		Onderdeel _60mH_JA = oDao.zoekOnderdeelExtraWaarde("60mH", 99.1);
		Onderdeel _110mH_A = oDao.zoekOnderdeelExtraWaarde("110mH", 99.1);
		Onderdeel _400mH_A = oDao.zoekOnderdeelExtraWaarde("400mH", 91.4);
		Onderdeel kogelJA = oDao.zoekOnderdeelWaarde("kogel", 6);
		Onderdeel discusJA = oDao.zoekOnderdeelWaarde("discus", 1.75);
		Onderdeel speerJA = oDao.zoekOnderdeelWaarde("speer", 800);
		Onderdeel kogelsl_A = oDao.zoekOnderdeelWaarde("kogelsl", 6);
		categorie.getOnderdelen().add(_50mH_JA);
		categorie.getOnderdelen().add(_60mH_JA);
		categorie.getOnderdelen().add(_110mH_A);
		categorie.getOnderdelen().add(_200mH);
		categorie.getOnderdelen().add(_400mH_A);
		categorie.getOnderdelen().add(kogelJA);
		categorie.getOnderdelen().add(discusJA);
		categorie.getOnderdelen().add(speerJA);
		categorie.getOnderdelen().add(kogelsl_A);
		categorie.getOnderdelen().add(polshoog);
		for(Onderdeel o: algemeneOnderdelen){
			o.getCategorien().add(categorie);	
		}
		for(Onderdeel o: algemeneBOnderdelen){
			o.getCategorien().add(categorie);	
		}
		for(Onderdeel o: algemeneAOnderdelen){
			o.getCategorien().add(categorie);	
		}
		em.merge(categorie);
		
	}
	

	private void vulPupillenOnderdelen(List<Onderdeel> onderdelen, EntityManager em) {
		CategorieDao dao = new CategorieDao();
		
		nl.altis.domein.Categorie categorie = dao.zoekCategorie("MPC");
		OnderdeelDao oDao = new OnderdeelDao();
		List<Onderdeel> algemeneOnderdelen = oDao.zoekOnderdelen("40m", "50m", "600m", "1000m", "1250m", "1500m","ver", "Hoog", "4 x 40m", "vortex");
		Onderdeel balC = oDao.zoekOnderdeelWaarde("bal", 80);
		Onderdeel kogelC = oDao.zoekOnderdeelWaarde("kogel", 2);
		Onderdeel _40mH = oDao.zoekOnderdeelExtraWaarde("40mH", 30);
		Onderdeel _60mH = oDao.zoekOnderdeelExtraWaarde("60mH", 45);
		categorie.getOnderdelen().addAll(algemeneOnderdelen);
		categorie.getOnderdelen().add(balC);
		categorie.getOnderdelen().add(kogelC);
		categorie.getOnderdelen().add(_40mH);
		categorie.getOnderdelen().add(_60mH);
		for(Onderdeel o: algemeneOnderdelen){
			o.getCategorien().add(categorie);	
		}
		em.merge(categorie);
		
		categorie = dao.zoekCategorie("MPD");
		categorie.getOnderdelen().addAll(algemeneOnderdelen);
		categorie.getOnderdelen().add(kogelC);
		categorie.getOnderdelen().add(balC);
		categorie.getOnderdelen().add(_40mH);
		categorie.getOnderdelen().add(_60mH);
		for(Onderdeel o: algemeneOnderdelen){
			o.getCategorien().add(categorie);	
		}
		em.merge(categorie);
		
		categorie = dao.zoekCategorie("JPD");
		categorie.getOnderdelen().addAll(algemeneOnderdelen);
		categorie.getOnderdelen().add(kogelC);
		categorie.getOnderdelen().add(balC);
		categorie.getOnderdelen().add(_40mH);
		categorie.getOnderdelen().add(_60mH);
		for(Onderdeel o: algemeneOnderdelen){
			o.getCategorien().add(categorie);	
		}
		em.merge(categorie);
		
		categorie = dao.zoekCategorie("JPC");
		categorie.getOnderdelen().addAll(algemeneOnderdelen);
		categorie.getOnderdelen().add(kogelC);
		categorie.getOnderdelen().add(balC);
		categorie.getOnderdelen().add(_40mH);
		categorie.getOnderdelen().add(_60mH);
		for(Onderdeel o: algemeneOnderdelen){
			o.getCategorien().add(categorie);	
		}
		em.merge(categorie);
		
		categorie = dao.zoekCategorie("MPB");
		Onderdeel balB = oDao.zoekOnderdeelWaarde("bal", 140);
		categorie.getOnderdelen().addAll(algemeneOnderdelen);
		categorie.getOnderdelen().add(kogelC);
		categorie.getOnderdelen().add(balB);
		categorie.getOnderdelen().add(_40mH);
		categorie.getOnderdelen().add(_60mH);
		for(Onderdeel o: algemeneOnderdelen){
			o.getCategorien().add(categorie);	
		}
		em.merge(categorie);
		
		categorie = dao.zoekCategorie("JPB");
		categorie.getOnderdelen().addAll(algemeneOnderdelen);
		categorie.getOnderdelen().add(kogelC);
		categorie.getOnderdelen().add(balB);
		categorie.getOnderdelen().add(_40mH);
		categorie.getOnderdelen().add(_60mH);
		for(Onderdeel o: algemeneOnderdelen){
			o.getCategorien().add(categorie);	
		}
		em.merge(categorie);
		
		categorie = dao.zoekCategorie("MPA");
		Onderdeel balA = oDao.zoekOnderdeelWaarde("bal", 170);
		Onderdeel _60m = oDao.zoekOnderdeel("60m");
		categorie.getOnderdelen().addAll(algemeneOnderdelen);
		categorie.getOnderdelen().add(_60m);
		categorie.getOnderdelen().add(_60mH);
		categorie.getOnderdelen().add(kogelC);
		categorie.getOnderdelen().add(balA);
		for(Onderdeel o: algemeneOnderdelen){
			o.getCategorien().add(categorie);	
		}
		em.merge(categorie);
		
		categorie = dao.zoekCategorie("JPA");
		categorie.getOnderdelen().addAll(algemeneOnderdelen);
		categorie.getOnderdelen().add(_60m);
		categorie.getOnderdelen().add(_60mH);
		categorie.getOnderdelen().add(kogelC);
		categorie.getOnderdelen().add(balA);
		for(Onderdeel o: algemeneOnderdelen){
			o.getCategorien().add(categorie);	
		}
		em.merge(categorie);
	}

	private List<Categorie> vulCategorien(EntityManager em) {
		Set<String> uniekeCategorieLijst = Categorie.getUniekeCategorieLijst();
		for (String cat: uniekeCategorieLijst){
			nl.altis.domein.Categorie categorie = new nl.altis.domein.Categorie();
			categorie.setCat(cat);
			categorie.setLangeNaam(Categorie.getLangeNaam(cat));
			em.persist(categorie);
		}
		
//		for(Categorie cat: Categorie.values()){
//			if (!(cat.getCategorie().contains("1") || cat.getCategorie().contains("2") || cat.getCategorie().contains("ini"))){
//				nl.altis.domein.Categorie categorie = new nl.altis.domein.Categorie();
//				categorie.setCat(cat.getAfkortingCategorie());
//				categorie.setLangeNaam(cat.getCategorie());
//				em.persist(categorie);	
//			}
//		}
		return null;
	}

	private void maakClubMeerkamp(EntityManager em) {
		em.getTransaction().begin();
		
		CategorieDao catDao = new CategorieDao();
		nl.altis.domein.Categorie catMC = catDao.zoekCategorie("MC");
		Onderdeel meerkamp = new Onderdeel();
		
		meerkamp.setKorteNaam("meerkamp");
		meerkamp.setLangeNaam("Club meerkamp");
		meerkamp.getCategorien().add(catMC);
		em.persist(meerkamp);
		catMC.getOnderdelen().add(meerkamp);
		em.merge(catMC);
		em.getTransaction().commit();
		
		em.getTransaction().begin();
		OnderdeelDao dao = new OnderdeelDao();
		Onderdeel mk = dao.zoekOnderdeel("meerkamp", "MC");
		
		Onderdeel kogel = dao.zoekOnderdeel("kogel", "MC");
		kogel.getMeerkamp().add(mk);
		Onderdeel _80M = dao.zoekOnderdeel("80m", "MC");
		 _80M.getMeerkamp().add(mk);
		Onderdeel _600M = dao.zoekOnderdeel("600m", "MC");
		_600M.getMeerkamp().add(mk);
		Onderdeel speer = dao.zoekOnderdeel("speer", "MC");
		speer.getMeerkamp().add(mk);
		Onderdeel ver = dao.zoekOnderdeel("ver", "MC");
		ver.getMeerkamp().add(mk);
		
		mk.getMeerkampOnderdelen().add(kogel);
		mk.getMeerkampOnderdelen().add(_80M);
		mk.getMeerkampOnderdelen().add(_600M);
		mk.getMeerkampOnderdelen().add(speer);
		mk.getMeerkampOnderdelen().add(ver);
		em.merge(_80M);
		em.merge(speer);
		em.merge(ver);
		em.merge(_600M);
		em.merge(kogel);
		em.merge(mk);
		

		em.getTransaction().commit();
		

	}
	

	private List<Onderdeel> vulOnderdelen(EntityManager em) {
		List<Onderdeel> onderdelenLijst = new ArrayList<>();
		OnderdelenLijst lijst = new OnderdelenLijst();
		String[][] onderdelen = lijst.getOnderdelen();
		for (String[] ond: onderdelen){
			Onderdeel onderdeel = new Onderdeel();
			onderdeel.setKorteNaam(ond[0]);
			onderdeel.setLangeNaam(ond[1]);
			if(ond.length == 3){
				onderdeel.setWaarde(new BigDecimal(ond[2]));	
			}
			if(ond.length == 4){
				onderdeel.setWaarde(new BigDecimal(ond[2]));
				onderdeel.setExtraWaarde(new BigDecimal(ond[3]));
			}
			if(ond.length == 5){
				onderdeel.setWaarde(new BigDecimal(ond[2]));
				onderdeel.setExtraWaarde(new BigDecimal(ond[3]));
				onderdeel.setToelichting(ond[4]);
			}
			onderdeel.setRegistratieDatum(new Date());
			onderdeel.setUser("fwn001");
			onderdelenLijst.add(onderdeel);
			em.persist(onderdeel);
		}
		return onderdelenLijst;
	}

}
