package nl.altis.domein;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="CATEGORIE")
public class Categorie {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int categorieId;

	private String cat;
	private String langeNaam;

	@ManyToMany
	@JoinTable(name = "CATEGORIE_ONDERDEEL", 
	   joinColumns = {      	@JoinColumn(name = "CATEGORIE_ID", nullable = false) }, 
	   inverseJoinColumns = {	@JoinColumn(name = "ONDERDEEL_ID", nullable = false) })
	List<Onderdeel> onderdelen = new ArrayList<>();

	public int getCategorieId() {
		return categorieId;
	}

	public void setCategorieId(int categorieId) {
		this.categorieId = categorieId;
	}

	public String getCat() {
		return cat;
	}

	public void setCat(String cat) {
		this.cat = cat;
	}

	public String getLangeNaam() {
		return langeNaam;
	}

	public void setLangeNaam(String langeNaam) {
		this.langeNaam = langeNaam;
	}

	public List<Onderdeel> getOnderdelen() {
		return onderdelen;
	}

	public void setOnderdelen(List<Onderdeel> onderdelen) {
		this.onderdelen = onderdelen;
	}

	@Override
	public String toString() {
		return "Categorie [cat=" + cat + "]";
	}

	
}
