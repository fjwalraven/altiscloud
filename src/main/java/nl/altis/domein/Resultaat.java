package nl.altis.domein;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import nl.altis.atletiek.nu.Ronde;

@Entity
@Table(name="RESULTAAT")
public class Resultaat {
	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	private int resultaatId;
	private String categorie;
	@OneToOne
	@JoinColumn(name="ONDERDEEL_ID")
	private Onderdeel onderdeel;
	@Enumerated(EnumType.STRING)
	private Ronde ronde;
	
	@Temporal(TemporalType.DATE)
	private Date datum;
	
	private String origineleWaarde;
	@Column(scale = 2, precision = 10)
	private BigDecimal afstand;
	@Column(scale = 2, precision = 10)
	private BigDecimal tijd;
	@Column(scale = 2, precision = 10)
	private BigDecimal punten;
	
	private String wind;
	@ManyToOne
	@JoinColumn(name="ATLEET_ID")
	private Atleet atleet;
	
	@ManyToOne
	@JoinColumn(name="WEDSTRIJD_ID")
	private Wedstrijd wedstrijd;

	public int getResultaatId() {
		return resultaatId;
	}

	public void setResultaatId(int resultaatId) {
		this.resultaatId = resultaatId;
	}

	public String getCategorie() {
		return categorie;
	}

	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}

	public Onderdeel getOnderdeel() {
		return onderdeel;
	}

	public void setOnderdeel(Onderdeel onderdeel) {
		this.onderdeel = onderdeel;
	}

	public Ronde getRonde() {
		return ronde;
	}

	public void setRonde(Ronde ronde) {
		this.ronde = ronde;
	}

	public BigDecimal getAfstand() {
		return afstand;
	}

	public void setAfstand(BigDecimal afstand) {
		this.afstand = afstand;
	}

	public BigDecimal getTijd() {
		return tijd;
	}

	public void setTijd(BigDecimal tijd) {
		this.tijd = tijd;
	}

	public BigDecimal getPunten() {
		return punten;
	}

	public void setPunten(BigDecimal punten) {
		this.punten = punten;
	}

	public Atleet getAtleet() {
		return atleet;
	}

	public void setAtleet(Atleet atleet) {
		this.atleet = atleet;
	}

	public Wedstrijd getWedstrijd() {
		return wedstrijd;
	}

	public void setWedstrijd(Wedstrijd wedstrijd) {
		this.wedstrijd = wedstrijd;
	}

	public String getWind() {
		return wind;
	}

	public void setWind(String wind) {
		this.wind = wind;
	}

	public String getOrigineleWaarde() {
		return origineleWaarde;
	}

	public void setOrigineleWaarde(String origineleWaarde) {
		this.origineleWaarde = origineleWaarde;
	}
	
	
	public Date getDatum() {
		return datum;
	}

	public void setDatum(Date datum) {
		this.datum = datum;
	}
	
	public String getDatumAsString() {
		DateFormat sorteerDatumFormatter = new SimpleDateFormat("dd-MM-yyyy");
		return sorteerDatumFormatter.format(datum);
	}

	public String toDisplayString() {
		StringBuilder sb = new StringBuilder();
		sb.append(origineleWaarde);
		sb.append(" --- ");
		sb.append(getAtleet().getNaam());
		sb.append("   ");
		DateFormat sorteerDatumFormatter = new SimpleDateFormat("yyyy-MM-dd");
		sb.append(sorteerDatumFormatter.format(datum));
		return sb.toString();
	}
	
	public String toDisplayStringMetWedstrijd() {
		StringBuilder sb = new StringBuilder();
		sb.append(fixedLengthString(origineleWaarde,8));
		sb.append(" --- ");
		sb.append(fixedLengthString(getAtleet().getNaam(),30));
		sb.append("   ");
		DateFormat sorteerDatumFormatter = new SimpleDateFormat("yyyy-MM-dd");
		sb.append(sorteerDatumFormatter.format(datum));
		sb.append("   ");
		sb.append(getWedstrijd().getNaam());
		sb.append("   ");
		sb.append(getWedstrijd().getPlaats());
		return sb.toString();
	}
	
	public String getJspDisplay() {
		StringBuilder sb = new StringBuilder();
		sb.append(fixedLengthString(origineleWaarde,8));
		sb.append(" --- ");
		sb.append(fixedLengthString(getAtleet().getNaam(),30));
		sb.append("   ");
		DateFormat sorteerDatumFormatter = new SimpleDateFormat("yyyy-MM-dd");
		sb.append(sorteerDatumFormatter.format(datum));
		sb.append("   ");
		sb.append(getWedstrijd().getNaam());
		sb.append("   ");
		sb.append(getWedstrijd().getPlaats());
		sb.append("<br/>");
		return sb.toString();
	}
	
	private String fixedLengthString(String string, int length) {
	    return String.format("%1$-"+length+ "s", string);
	}

	@Override
	public String toString() {
		return "Resultaat [resultaatId=" + resultaatId + ", categorie=" + categorie + ", onderdeel=" + onderdeel
				+ ", ronde=" + ronde + ", datum=" + datum + ", origineleWaarde=" + origineleWaarde + ", afstand="
				+ afstand + ", tijd=" + tijd + ", punten=" + punten + ", wind=" + wind + ", atleet=" + atleet
				+ ", wedstrijd=" + wedstrijd + "]";
	}

}
