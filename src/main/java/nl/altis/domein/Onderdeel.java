package nl.altis.domein;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Entity implementation class for Entity: Onderdeel
 *
 */
@Entity
@Table(name="ONDERDEEL")
public class Onderdeel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int onderdeelId;

	private String korteNaam;
	private String langeNaam;
	@Column(scale = 2, precision = 10)
	private BigDecimal waarde;
	@Column(scale = 2, precision = 10)
	private BigDecimal extraWaarde;

	@Temporal(TemporalType.TIMESTAMP)
	private Date registratieDatum;
	private String user;

	private String toelichting;

	@ManyToMany(mappedBy = "onderdelen")
	List<Categorie> categorien = new ArrayList<>();

	@ManyToOne
	private Wedstrijd wedstrijd;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "MEERKAMP", 
	           joinColumns = { @JoinColumn(name = "MEERKAMPONDERDEEL_ID", referencedColumnName = "onderdeelId", nullable = false) }, 
		inverseJoinColumns = { @JoinColumn(name = "MEERKAMP_ID", referencedColumnName = "onderdeelId", nullable = false) })
	private List<Onderdeel> meerkamp = new ArrayList<>();;

	@ManyToMany(mappedBy = "meerkamp", fetch = FetchType.EAGER)
	private List<Onderdeel> meerkampOnderdelen = new ArrayList<>();

	@OneToOne(mappedBy = "onderdeel")
	Resultaat resultaat;

	public Onderdeel() {
		super();
	}

	public int getOnderdeelId() {
		return this.onderdeelId;
	}

	public void setOnderdeelId(int onderdeelId) {
		this.onderdeelId = onderdeelId;
	}

	// public Onderdeel getMeerkamp() {
	// return meerkamp;
	// }
	// public void setMeerkamp(Onderdeel meerkamp) {
	// this.meerkamp = meerkamp;
	// }

	public List<Onderdeel> getMeerkampOnderdelen() {
		return meerkampOnderdelen;
	}
	
	public List<Integer> getMeerkampOnderdelenIds() {
		List<Integer> ids = new ArrayList<>();
		List<Onderdeel> mko = getMeerkampOnderdelen();
		if(mko.size()>0){
			for(Onderdeel o: mko){
				ids.add(o.getOnderdeelId());
			}	
		}
		return ids;
	}


	public List<Onderdeel> getMeerkamp() {
		return meerkamp;
	}

	public void setMeerkamp(List<Onderdeel> meerkamp) {
		this.meerkamp = meerkamp;
	}

	public void setMeerkampOnderdelen(List<Onderdeel> meerkampOnderdelen) {
		this.meerkampOnderdelen = meerkampOnderdelen;
	}

	public Wedstrijd getWedstrijd() {
		return wedstrijd;
	}

	public void setWedstrijd(Wedstrijd wedstrijd) {
		this.wedstrijd = wedstrijd;
	}

	public Resultaat getResultaat() {
		return resultaat;
	}

	public void setResultaat(Resultaat resultaat) {
		this.resultaat = resultaat;
	}

	public String getKorteNaam() {
		return korteNaam;
	}

	public void setKorteNaam(String korteNaam) {
		this.korteNaam = korteNaam;
	}

	public String getLangeNaam() {
		return langeNaam;
	}

	public void setLangeNaam(String langeNaam) {
		this.langeNaam = langeNaam;
	}

	public BigDecimal getWaarde() {
		return waarde;
	}

	public void setWaarde(BigDecimal waarde) {
		this.waarde = waarde;
	}

	public Date getRegistratieDatum() {
		return registratieDatum;
	}

	public void setRegistratieDatum(Date registratieDatum) {
		this.registratieDatum = registratieDatum;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public BigDecimal getExtraWaarde() {
		return extraWaarde;
	}

	public void setExtraWaarde(BigDecimal extraWaarde) {
		this.extraWaarde = extraWaarde;
	}

	public String getToelichting() {
		return toelichting;
	}

	public void setToelichting(String toelichting) {
		this.toelichting = toelichting;
	}

	public List<Categorie> getCategorien() {
		return categorien;
	}

	public void setCategorien(List<Categorie> categorien) {
		this.categorien = categorien;
	}

	@Override
	public String toString() {
		return "Onderdeel [onderdeelId=" + onderdeelId + ", korteNaam=" + korteNaam + ", langeNaam=" + langeNaam
				+ ", waarde=" + waarde + ", extraWaarde=" + extraWaarde + ", registratieDatum=" + registratieDatum
				+ ", user=" + user + ", toelichting=" + toelichting + ", wedstrijd=" + wedstrijd + "]";
	}

	

}
