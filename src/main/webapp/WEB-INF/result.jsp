<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>JSP Page</title>
</head>

<body>
	<c:set var="context" value="${pageContext.request.contextPath}" />
	<h1>Resultaten Altis Outdoor 2018</h1>

		<table style="text-align: center; width: 100%;" border="1"
		cellpadding="2" cellspacing="2">

		<tbody>
			<tr>
				<td	style="vertical-align: top; background-color: rgb(204, 204, 204); width:20%;">Pupillen<br></td>
				<td	style="vertical-align: top; background-color: rgb(204, 204, 204); width:20%;">Junioren<br></td>
				<td	style="vertical-align: top; background-color: rgb(204, 204, 204); width:20%;">Senioren<br></td>
				<td	style="vertical-align: top; background-color: rgb(204, 204, 204); width:20%;">Masters<br></td>
			</tr>

			<c:set var="pupillen" value="${pup}"></c:set>
			<c:set var="junioren" value="${jun}"></c:set>
			<c:set var="senioren" value="${sen}"></c:set>
			<c:set var="masters" value="${mas}"></c:set>
			
			<c:forEach begin="1" end="${max}" varStatus="status">
				<tr>
					<td style="vertical-align: top; width:20%;"><a href="${context}/Altis?cat=${pupillen[status.index]}">${pupillen[status.index]}</a><br></td>
					<td style="vertical-align: top; width:20%;"><a href="${context}/Altis?cat=${junioren[status.index]}">${junioren[status.index]}</a><br></td>
					<td style="vertical-align: top; width:20%;"><a href="${context}/Altis?cat=${senioren[status.index]}">${senioren[status.index]}</a><br></td>
					<td style="vertical-align: top; width:20%;"><a href="${context}/Altis?cat=${masters[status.index]}">${masters[status.index]}</a><br></td>
				</tr>
			</c:forEach>

		</tbody>
	</table>
<hr>
<form method="POST" action="${context}/Altis">
	Zoek op naam: <input type="text" name="naam" /> <br> <br> 
	
	<input type="SUBMIT">
</form>

</body>
</html>
