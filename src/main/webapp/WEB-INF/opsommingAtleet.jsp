<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>JSP Page</title>
</head>

<body>
	<c:set var="context" value="${pageContext.request.contextPath}" />
<h2>${resultaat.atleet.naam}</h2>
<c:forEach var="ond" items="${keySet}">
	<h2>${ond.langeNaam}</h2>
	<table style="text-align: center; width: 100%;" border="1"
		cellpadding="2" cellspacing="2">
		<c:forEach var="resultaat" items="${resMap[ond]}" varStatus="loop">
		<c:if test="${loop.index % 2 == 0}">
			<tr>
				<td>${resultaat.origineleWaarde}</td>
				<td>${loop.index + 1}</td>
				<td>${resultaat.datumAsString}</td>
				<td>${resultaat.wedstrijd.naam}</td>
				<td>${resultaat.wedstrijd.plaats}</td>
			</tr>
		</c:if>
		<c:if test="${loop.index % 2 != 0}">
			<tr style="background-color: rgb(229, 229, 229);">
				<td>${resultaat.origineleWaarde}</td>
				<td>${loop.index + 1}</td>
				<td>${resultaat.datumAsString}</td>
				<td>${resultaat.wedstrijd.naam}</td>
				<td>${resultaat.wedstrijd.plaats}</td>
			</tr>
		</c:if>
		</c:forEach>

	</table>
</c:forEach>


</body>
</html>